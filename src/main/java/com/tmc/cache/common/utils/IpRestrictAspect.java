package com.tmc.cache.common.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;


@Aspect
@Component
public class IpRestrictAspect{
    @Around(value = "@annotation(IpRestricted)", argNames = "jp, IpRestricted")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint, IpRestricted ipRestricted) throws Throwable {
        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;
        Object obj = joinPoint.getThis(); // get the object
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod(); // get the origin method
//        Method target = obj.getClass().getMethod(ipRestricted.value(), method.getParameterTypes()); // get the delegate method
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        System.out.println("========================== request ip : " + request.getRemoteAddr());
        /*if (!request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")) {
            throw new Exception("Ip not allow");
        }*/

        System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }
}
