package com.tmc.cache.common.utils;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IpRestricted {


}
