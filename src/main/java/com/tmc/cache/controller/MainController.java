package com.tmc.cache.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tmc.cache.common.utils.IpRestricted;
import com.tmc.cache.domain.*;
import com.tmc.cache.domain.dto.UserWalletUpdateDTO;
import com.tmc.cache.service.CacheService;
import com.tmc.cache.service.UserAccountProcessingService;
import com.tmc.cache.service.UserAccountService;
import com.tmc.cache.service.UserWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.List;

/**
 * sovannarith on 9/5/18
 */
@RestController
@RequestMapping("/find")
public class MainController {

    @Autowired
    private CacheService cacheService;

    @Autowired
    public UserAccountProcessingService userAccountProcessingService;

    @Autowired
    public UserAccountService userAccountService;

    @Autowired
    public UserWalletService userWalletService;

    @GetMapping("sys_pre/{variable}")
    @IpRestricted
    public List<SystemPreference> getSysPreferenceByVariableContaining(@PathVariable("variable") String variable, HttpServletRequest request) {
        return cacheService.getSysPreferenceByVariableContaining(variable);
    }

    @GetMapping("sys_pre/value/{value}")
    @IpRestricted
    public List<SystemPreference> getSysPreferenceByValueContaining(@PathVariable("value") String value, HttpServletRequest request) {
        return cacheService.getSysPreferenceByValueContaining(value);
    }

    @GetMapping("sys_msg/{code}")
    @IpRestricted
    public List<SysMessage> getSysMessageByCodeContaining(@PathVariable("code") String code) {
        return cacheService.getSysMessageByCodeContaining(code);
    }

    @GetMapping("sys_msg/message/{message}")
    @IpRestricted
    public List<SysMessage> getSysMessageByMessageContaining(@PathVariable("message") String message) {
        return cacheService.findByMessageContainingIgnoreCase(message);
    }

    @PostMapping(value = "/update/user_processing/{userAccountId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    @IpRestricted
    public int updateUserProcessing(@PathVariable("userAccountId") Integer userAccountId) throws JsonProcessingException {
        return userAccountProcessingService.updateUserProcessing(userAccountId);
    }

    @GetMapping("/user_processing/{userAccountId}")
    @IpRestricted
    public UserAccountProcessing getUserAccountProcessingByUserAccountId(@PathVariable("userAccountId") Integer userAccountId) {
        return userAccountProcessingService.getUserAccountProcessingByUserAccountId(userAccountId);
    }

    @GetMapping("/user_account_card_profile/{userAccountId}")
    @IpRestricted
    public UserAccountCardProfile getUserAccountCardProfilesByUserAccountId(@PathVariable("userAccountId") Integer userAccountId) {
        return userAccountService.getUserAccountCardProfileByUserAccountId(userAccountId);
    }

    @GetMapping("/user_account_profiles/{userAccountId}")
    @IpRestricted
    public UserAccountsProfiles getUserAccountProfilesByUserAccountId(@PathVariable("userAccountId") Integer userAccountId) {
        return userAccountService.getUserAccountsProfilesByUserAccountId(userAccountId);
    }

    @GetMapping("/user_wallet/{userAccountId}")
    @IpRestricted
    public List<UserWallet> getUserWalletByUserAccountId(@PathVariable("userAccountId") Integer userAccountId){
        return userWalletService.getUserWalletByUserAccountId(userAccountId);
    }

    @PostMapping(value = "/update/user_wallet", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    @IpRestricted
    public int updateUserProcessing(@RequestBody UserWalletUpdateDTO userWalletUpdateDTO) throws JsonProcessingException {
        return userWalletService.updateUserWalletByUserAccountIdAndCurrencyId(userWalletUpdateDTO);
    }
}
