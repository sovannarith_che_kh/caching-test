package com.tmc.cache.controller;

import com.tmc.cache.domain.SysActivateDevices;
import com.tmc.cache.domain.dto.SysActivateDeviceDTO;
import com.tmc.cache.service.OtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/otp")
public class OtpController {

    @Autowired
    private OtpService otpService;

    @GetMapping("/phone")
    public List<SysActivateDevices> getByPhone(@RequestParam String phone, @RequestParam String deviceType) {
        return otpService.getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(new SysActivateDeviceDTO(phone, deviceType));
    }

    @GetMapping("/card")
    public List<SysActivateDevices> getByCard(@RequestParam String card, @RequestParam String deviceType) {
        return otpService.getSysActivateDevicesByCardIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(new SysActivateDeviceDTO(card, deviceType, null));
    }

    @GetMapping("/account")
    public List<SysActivateDevices> getByAccount(@RequestParam Integer account, @RequestParam String deviceType) {
        return otpService.getSysActivateDevicesByUserAccountIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(new SysActivateDeviceDTO(account, deviceType, null, null));
    }

}
