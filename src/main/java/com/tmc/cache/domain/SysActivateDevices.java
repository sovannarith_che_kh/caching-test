package com.tmc.cache.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "sys_activate_devices")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SysActivateDevices implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "card__id")
	private String cardId;
	
	@Column(name = "device_id")
	private String deviceId;
	
	@Column(name = "device_type")
	private String deviceType;
	
	@Column(name = "security_code")
	private String securityCode;
	
	@Column(name = "datetime_expire_security_code")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date dateTimeExpireSecurityCode;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "created_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private Date createdAt;
	
	@Column(name = "activated_at")
	private Date activatedAt;
	
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "request_gateway__id")
	private String requestGatewayId;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "user_account__id")
	private Integer userAccountId;
	
	@Column(name = "app_version")
	private String appVersion;
	
	@Column(name = "active_by")
	private String activeBy;
	
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getUserAccountId() {
		return userAccountId;
	}
	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public Date getDateTimeExpireSecurityCode() {
		return dateTimeExpireSecurityCode;
	}
	public void setDateTimeExpireSecurityCode(Date dateTimeExpireSecurityCode) {
		this.dateTimeExpireSecurityCode = dateTimeExpireSecurityCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getActivatedAt() {
		return activatedAt;
	}
	public void setActivatedAt(Date activatedAt) {
		this.activatedAt = activatedAt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRequestGatewayId() {
		return requestGatewayId;
	}
	public void setRequestGatewayId(String requestGatewayId) {
		this.requestGatewayId = requestGatewayId;
	}
	public String getActiveBy() {
		return activeBy;
	}
	public void setActiveBy(String activeBy) {
		this.activeBy = activeBy;
	}
	
}
