package com.tmc.cache.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_messages")
public class SysMessage {

	@Id
	@Column(name = "code")
	private String code;

	@Column(name = "message")
	private String message;

	@Column(name = "message_khmer")
	private String messageKhmer;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageKhmer() {
		return messageKhmer;
	}

	public void setMessageKhmer(String messageKhmer) {
		this.messageKhmer = messageKhmer;
	}

}