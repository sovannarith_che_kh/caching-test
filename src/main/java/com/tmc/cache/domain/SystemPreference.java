package com.tmc.cache.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sys_system_preference")
public class SystemPreference implements Serializable {
	@Id
	@Column(name = "variable")
	private String variable;

	@Column(name = "data_type")
	private String dataType;

	@Column(name = "value")
	private String value;

	@Column(name = "is_allow_update")
	private String isAllowUpdate;

	@Column(name = "sys_preference_type")
	private String sysPreferenceType;

	@PostLoad
    public void calculateAge() {
		System.out.println("load");
	}
	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getIsAllowUpdate() {
		return isAllowUpdate;
	}

	public void setIsAllowUpdate(String isAllowUpdate) {
		this.isAllowUpdate = isAllowUpdate;
	}

	public String getSysPreferenceType() {
		return sysPreferenceType;
	}

	public void setSysPreferenceType(String sysPreferenceType) {
		this.sysPreferenceType = sysPreferenceType;
	}
}
