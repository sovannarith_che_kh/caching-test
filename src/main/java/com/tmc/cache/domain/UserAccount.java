package com.tmc.cache.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_account")
public class UserAccount implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @Column(name = "user_account_id")
    private Integer userAccountId;

    @Column(name = "account_type__id")
    private Integer accountTypeId;

    @Column(name = "require_change_password", insertable = false)
    private String requireChangePassword;

    @Column(name = "remember_token")
    private String rememberToken;

    @Column(name = "status")
    private String status;

    @Column(name = "num_fail_authenticate_pin")
    private Integer numFailAuthenticatePin;

    @Column(name = "num_fail_security_code")
    private Integer numFailSecurityCode;

    @Column(name = "type_account")
    private String typeAccount;

    @Column(name = "code")
    private String code;

    @Column(name = "sequence_number")
    private Integer sequenceNumber;

    @Column(name = "parent_account_id")
    private Integer parentAccountId;

    @Column(name = "reference_account_id")
    private Integer referenceAccountId;

    @Column(name = "atr1_name")
    private String atr1Name;

    @Column(name = "atr1_value")
    private String atr1Value;

    @Column(name = "atr4_name")
    private String atr4Name;

    @Column(name = "atr4_value")
    private String atr4Value;

    @Column(name = "is_block_some_amount")
    private boolean isBlockSomeAmount;

    public String getAtr4Name() {
        return atr4Name;
    }

    public void setAtr4Name(String atr4Name) {
        this.atr4Name = atr4Name;
    }

    public String getAtr4Value() {
        return atr4Value;
    }

    public void setAtr4Value(String atr4Value) {
        this.atr4Value = atr4Value;
    }

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "staff__id")
    private Integer staffId;

    @Column(name = "created_by_user_profile__id")
    private Integer createdByProfileId;

    @Column(name = "updated_at")
    private Date updatedAt;

    @Column(name = "updated_by_staff__id")
    private Integer updatedByStaffId;

    @Column(name = "password2")
    private String password2;

    @Column(name = "password3")
    private String password3;

    @Column(name = "password")
    private String password;


    @Column(name = "reason_os")
    private String reasonOs;

    //@Column(name = "is_require_change_password",insertable = false)  ---and set isRequireChangePassword ="Y"
    @Column(name = "is_require_change_password")
    private String isRequireChangePassword = "Y";

    // Updated 11 Ague 2015
    @Column(name = "is_created_by_user")
    private String isCreatedByUser;

    @Column(name = "created_by_user_account__id")
    private Integer createdByUserAccountId;

    @Column(name = "is_updated_by_user")
    private String isUpdatedByUser;

    @Column(name = "updated_by_user_account__id")
    private Integer updatedByUserAccountid;

    @Column(name = "updated_by_user_profile__id")
    private Integer updatedByUserProfileId;


    @Column(name = "atr2_name")
    private String atr2Name;

    @Column(name = "atr2_value")
    private String atr2Value;

    @Column(name = "atr3_name")
    private String atr3Name;

    @Column(name = "atr3_value")
    private String atr3Value;

    @Column(name = "num_cash_out_times", insertable = false)
    private Integer numTimesCashOut;

    @Column(name = "is_payroll_account", insertable = false)
    String isPayrollAccount;

    @Column(name = "company_account_id")
    String companyAccountId;

    @Column(name = "branch_digits")
    String branchDigits;

    @Column(name = "cash_out_time_same_tc_perday")
    Integer cashOutTimeSameTcPerday;

    @Column(name = "agent_type")
    private String agentType;

    @Column(name = "num_fail_otp")
    private Integer numFailOTP;

    @Column(name = "reason_suspend")
    private String reasonSuspend;

    @Column(name = "suspend_date")
    private Date suspendDate;

    @Column(name = "register_by")
    private String registerBy;

    public String getRegisterBy() {
        return registerBy;
    }

    public void setRegisterBy(String registerBy) {
        this.registerBy = registerBy;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }


    public String getBranchDigits() {
        return branchDigits;
    }

    public void setBranchDigits(String branchDigits) {
        this.branchDigits = branchDigits;
    }

    public String getCompanyAccountId() {
        return companyAccountId;
    }

    public void setCompanyAccountId(String companyAccountId) {
        this.companyAccountId = companyAccountId;
    }

    public String getIsPayrollAccount() {
        return isPayrollAccount;
    }

    public void setIsPayrollAccount(String isPayrollAccount) {
        this.isPayrollAccount = isPayrollAccount;
    }

    public Integer getNumTimesCashOut() {
        return numTimesCashOut;
    }

    public void setNumTimesCashOut(Integer numTimesCashOut) {
        this.numTimesCashOut = numTimesCashOut;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getAtr2Name() {
        return atr2Name;
    }

    public void setAtr2Name(String atr2Name) {
        this.atr2Name = atr2Name;
    }

    public String getAtr2Value() {
        return atr2Value;
    }

    public void setAtr2Value(String atr2Value) {
        this.atr2Value = atr2Value;
    }

    public String getAtr3Name() {
        return atr3Name;
    }

    public void setAtr3Name(String atr3Name) {
        this.atr3Name = atr3Name;
    }

    public String getAtr3Value() {
        return atr3Value;
    }

    public void setAtr3Value(String atr3Value) {
        this.atr3Value = atr3Value;
    }

    public String getPassword3() {
        return password3;
    }

    public void setPassword3(String password3) {
        this.password3 = password3;
    }

    public Integer getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Integer userAccountId) {
        this.userAccountId = userAccountId;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getRequireChangePassword() {
        return requireChangePassword;
    }

    public void setRequireChangePassword(String requireChangePassword) {
        this.requireChangePassword = requireChangePassword;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNumFailAuthenticatePin() {
        return numFailAuthenticatePin;
    }

    public void setNumFailAuthenticatePin(Integer numFailAuthenticatePin) {
        this.numFailAuthenticatePin = numFailAuthenticatePin;
    }

    public Integer getNumFailSecurityCode() {
        return numFailSecurityCode;
    }

    public void setNumFailSecurityCode(int numFailSecurityCode) {
        this.numFailSecurityCode = numFailSecurityCode;
    }

    public String getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(String typeAccount) {
        this.typeAccount = typeAccount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getAtr1Name() {
        return atr1Name;
    }

    public void setAtr1Name(String atr1Name) {
        this.atr1Name = atr1Name;
    }

    public String getAtr1Value() {
        return atr1Value;
    }

    public void setAtr1Value(String atr1Value) {
        this.atr1Value = atr1Value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedByProfileId() {
        return createdByProfileId;
    }

    public void setCreatedByProfileId(Integer createdByProfileId) {
        this.createdByProfileId = createdByProfileId;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedByStaffId() {
        return updatedByStaffId;
    }

    public void setUpdatedByStaffId(Integer updatedByStaffId) {
        this.updatedByStaffId = updatedByStaffId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public Integer getParentAccountId() {
        return parentAccountId;
    }

    public void setParentAccountId(Integer parentAccountId) {
        this.parentAccountId = parentAccountId;
    }

    public Integer getReferenceAccountId() {
        return referenceAccountId;
    }

    public void setReferenceAccountId(Integer referenceAccountId) {
        this.referenceAccountId = referenceAccountId;
    }

    public void setNumFailSecurityCode(Integer numFailSecurityCode) {
        this.numFailSecurityCode = numFailSecurityCode;
    }

    public String getReasonOs() {
        return reasonOs;
    }

    public void setReasonOs(String reason) {
        this.reasonOs = reason;
    }

    public String getIsCreatedByUser() {
        return isCreatedByUser;
    }

    public void setIsCreatedByUser(String isCreatedByUser) {
        this.isCreatedByUser = isCreatedByUser;
    }

    public Integer getCreatedByUserAccountId() {
        return createdByUserAccountId;
    }

    public void setCreatedByUserAccountId(Integer createdByUserAccountId) {
        this.createdByUserAccountId = createdByUserAccountId;
    }

    public String getIsUpdatedByUser() {
        return isUpdatedByUser;
    }

    public void setIsUpdatedByUser(String isUpdatedByUser) {
        this.isUpdatedByUser = isUpdatedByUser;
    }

    public Integer getUpdatedByUserAccountid() {
        return updatedByUserAccountid;
    }

    public void setUpdatedByUserAccountid(Integer updatedByUserAccountid) {
        this.updatedByUserAccountid = updatedByUserAccountid;
    }

    public Integer getUpdatedByUserProfileId() {
        return updatedByUserProfileId;
    }

    public void setUpdatedByUserProfileId(Integer updatedByUserProfileId) {
        this.updatedByUserProfileId = updatedByUserProfileId;
    }

    @Column(name = "finger_image")
    private String fingerImage;
    @Column(name = "finger_platform")
    private String fingerPlatform;

    public String getFingerImage() {
        return fingerImage;
    }

    public void setFingerImage(String fingerImage) {
        this.fingerImage = fingerImage;
    }

    public String getFingerPlatform() {
        return fingerPlatform;
    }

    public void setFingerPlatform(String fingerPlatform) {
        this.fingerPlatform = fingerPlatform;
    }

    @Column(name = "is_check")
    private String isCheck;

    public String getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(String isCheck) {
        this.isCheck = isCheck;
    }

    @Column(name = "user_account_classify__id")
    private Integer userAccountClassifyId;

    public Integer getUserAccountClassifyId() {
        return userAccountClassifyId;
    }

    public void setUserAccountClassifyId(Integer userAccountClassifyId) {
        this.userAccountClassifyId = userAccountClassifyId;
    }

    @Column(name = "customer_id")
    private String customerId;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Column(name = "atr5_name")
    private String atr5Name;

    @Column(name = "atr5_value")
    private String atr5Value;

    public String getAtr5Name() {
        return atr5Name;
    }

    public void setAtr5Name(String atr5Name) {
        this.atr5Name = atr5Name;
    }

    public String getAtr5Value() {
        return atr5Value;
    }

    public void setAtr5Value(String atr5Value) {
        this.atr5Value = atr5Value;
    }

    public Integer getCashOutTimeSameTcPerday() {
        return cashOutTimeSameTcPerday;
    }

    public void setCashOutTimeSameTcPerday(Integer cashOutTimeSameTcPerday) {
        this.cashOutTimeSameTcPerday = cashOutTimeSameTcPerday;
    }

    public String getIsRequireChangePassword() {
        return isRequireChangePassword;
    }

    public void setIsRequireChangePassword(String isRequireChangePassword) {
        this.isRequireChangePassword = isRequireChangePassword;
    }

    public boolean isBlockSomeAmount() {
        return isBlockSomeAmount;
    }

    public void setBlockSomeAmount(boolean isBlockSomeAmount) {
        this.isBlockSomeAmount = isBlockSomeAmount;
    }

    public Integer getNumFailOTP() {
        return numFailOTP;
    }

    public void setNumFailOTP(Integer numFailOTP) {
        this.numFailOTP = numFailOTP;
    }

    public String getReasonSuspend() {
        return reasonSuspend;
    }

    public void setReasonSuspend(String reasonSuspend) {
        this.reasonSuspend = reasonSuspend;
    }

    public Date getSuspendDate() {
        return suspendDate;
    }

    public void setSuspendDate(Date suspendDate) {
        this.suspendDate = suspendDate;
    }

}