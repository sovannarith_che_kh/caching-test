package com.tmc.cache.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_account_card_profile")
public class UserAccountCardProfile implements Serializable {

	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_account_card_profile_id")
	private Integer userAccountCardProfileId;

	@OneToOne
	@JoinColumn(name = "user_account__id")
	private UserAccount userAccount;

	@Column(name = "user_account__id", insertable = false, updatable = false)
	private Integer userAccountId;

	@OneToOne
	@JoinColumn(name = "user_profile__id")
	private UserProfile userProfile;

	@Column(name = "card__id")
	private String cardId;

	@Column(name = "is_user_profile_change")
	private String isUserProfileChange;

	@Column(name = "is_card_change")
	private String isCardChange;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "staff__id")
	private Integer staffId;

	@Column(name = "created_by_user_profile__id")
	private Integer createdByProfileId;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by_staff__id")
	private Integer updatedByStaffId;

	@Column(name ="status")
	private String status;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "updated_by_user_profile__id")
	private Integer updatedByUserProfileId;

	@Column(name = "password2")
	private String password2;
	
	@Column(name = "password3")
	private String password3;

	@Column(name = "finger_image")
	private String fingerImage;

	@Column(name = "finger_platform")
	private String fingerPlatform;

	public Integer getUserAccountCardProfileId() {
		return userAccountCardProfileId;
	}

	public void setUserAccountCardProfileId(Integer userAccountCardProfileId) {
		this.userAccountCardProfileId = userAccountCardProfileId;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getIsUserProfileChange() {
		return isUserProfileChange;
	}

	public void setIsUserProfileChange(String isUserProfileChange) {
		this.isUserProfileChange = isUserProfileChange;
	}

	public String getIsCardChange() {
		return isCardChange;
	}

	public void setIsCardChange(String isCardChange) {
		this.isCardChange = isCardChange;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Integer getCreatedByProfileId() {
		return createdByProfileId;
	}

	public void setCreatedByProfileId(Integer createdByProfileId) {
		this.createdByProfileId = createdByProfileId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedByStaffId() {
		return updatedByStaffId;
	}

	public void setUpdatedByStaffId(Integer updatedByStaffId) {
		this.updatedByStaffId = updatedByStaffId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUpdatedByUserProfileId() {
		return updatedByUserProfileId;
	}

	public void setUpdatedByUserProfileId(Integer updatedByUserProfileId) {
		this.updatedByUserProfileId = updatedByUserProfileId;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getPassword3() {
		return password3;
	}

	public void setPassword3(String password3) {
		this.password3 = password3;
	}

	public String getFingerImage() {
		return fingerImage;
	}

	public void setFingerImage(String fingerImage) {
		this.fingerImage = fingerImage;
	}

	public String getFingerPlatform() {
		return fingerPlatform;
	}

	public void setFingerPlatform(String fingerPlatform) {
		this.fingerPlatform = fingerPlatform;
	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}

	@Override
	public String toString() {
		return "UserAccountCardProfile{" +
				"userAccountCardProfileId=" + userAccountCardProfileId +
				", userAccount=" + userAccount +
				", userProfile=" + userProfile +
				", cardId='" + cardId + '\'' +
				", isUserProfileChange='" + isUserProfileChange + '\'' +
				", isCardChange='" + isCardChange + '\'' +
				", createdAt=" + createdAt +
				", staffId=" + staffId +
				", createdByProfileId=" + createdByProfileId +
				", updatedAt=" + updatedAt +
				", updatedByStaffId=" + updatedByStaffId +
				", status='" + status + '\'' +
				", password='" + password + '\'' +
				", updatedByUserProfileId=" + updatedByUserProfileId +
				", password2='" + password2 + '\'' +
				", password3='" + password3 + '\'' +
				", fingerImage='" + fingerImage + '\'' +
				", fingerPlatform='" + fingerPlatform + '\'' +
				'}';
	}
}
