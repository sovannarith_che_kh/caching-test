package com.tmc.cache.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_accounts_profiles")
public class UserAccountsProfiles implements Serializable {

	/**
	 * @author sovannarith.che
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_account_profile_id")
	private Integer userAccountProfileId;

	@OneToOne
	@JoinColumn(name = "user_account__id")
	private UserAccount userAccount;

	@Column(name = "user_account__id", insertable = false, updatable = false)
	private Integer userAccountId;

	@OneToOne
	@JoinColumn(name = "user_profile__id")
	private UserProfile userProfile;

	@Column(name = "phone1_bind")
	private String phone1Bind;

	@Column(name = "phone2_bind")
	private String phone2Bind;

	@Column(name = "is_phone1_bind")
	private String isPhone1Bind;

	@Column(name = "is_phone2_bind")
	private String isPhone2Bind;

	@Column(name = "status")
	private String status;
	
	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "staff__id")
	private Integer staffId;

	@Column(name = "created_by_user_profile__id")
	private Integer createdByProfileId;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by_staff__id")
	private Integer updatedByStaffId;

	public Integer getUserAccountProfileId() {
		return userAccountProfileId;
	}

	public void setUserAccountProfileId(Integer userAccountProfileId) {
		this.userAccountProfileId = userAccountProfileId;
	}

	public String getPhone1Bind() {
		return phone1Bind;
	}

	public void setPhone1Bind(String phone1Bind) {
		this.phone1Bind = phone1Bind;
	}

	public String getPhone2Bind() {
		return phone2Bind;
	}

	public void setPhone2Bind(String phone2Bind) {
		this.phone2Bind = phone2Bind;
	}

	public String getIsPhone1Bind() {
		return isPhone1Bind;
	}

	public void setIsPhone1Bind(String isPhone1Bind) {
		this.isPhone1Bind = isPhone1Bind;
	}

	public String getIsPhone2Bind() {
		return isPhone2Bind;
	}

	public void setIsPhone2Bind(String isPhone2Bind) {
		this.isPhone2Bind = isPhone2Bind;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getCreatedByProfileId() {
		return createdByProfileId;
	}

	public void setCreatedByProfileId(Integer createdByProfileId) {
		this.createdByProfileId = createdByProfileId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedByStaffId() {
		return updatedByStaffId;
	}

	public void setUpdatedByStaffId(Integer updatedByStaffId) {
		this.updatedByStaffId = updatedByStaffId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}
}
