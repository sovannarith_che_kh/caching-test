package com.tmc.cache.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_profile")
public class UserProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_profile_id")
	private Integer userProfileId;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "occupation")
	private String occupation;

	@Column(name = "shop_name")
	private String shopName;

	@Column(name = "identify_id")
	private String identifyId;

	@Column(name = "identify_type")
	private String identifyType;

	@Column(name = "status")
	private String status;

	@Column(name = "khan__code")
	private String khanCode;

	@Column(name = "province__code")
	private String provinceCode;

	@Column(name = "commune_code")
	private String communeCode;

	@Column(name = "village_code")
	private String villageCode;

	@Column(name = "address")
	private String address;

	@Column(name = "phone1")
	private String phone1;

	@Column(name = "phone2")
	private String phone2;

	@Column(name = "contact_phone")
	private String contactPhone;

	@Column(name = "email")
	private String email;

	@Column(name = "contract_file")
	private String contractFile;

	@Column(name = "contract_date")
	private Date contractDate;

	@Column(name = "parent_id")
	private Integer parentId;

	@Column(name = "user_profile_ref_id")
	private Integer userProfileRefId;

	@Column(name = "atr1_name")
	private String atr1Name;

	@Column(name = "atr1_value")
	private String atr1Value;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "staff__id")
	private Integer staffId;

	@Column(name = "created_by_profile__id")
	private Integer createdByProfileId;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by_staff__id")
	private Integer updatedByStaffId;

	@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Column(name="image_name")
	private String imageName;
	
	// Updated 11 Ague 2015	
	@Column(name="latitude")
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;
	
	@Column(name="is_created_by_user")
	private String isCreatedByUser;
	
	@Column(name="created_by_user_account__id")
	private Integer createdByUserAccountId;
	
	@Column(name="is_updated_by_user")
	private String isUpdatedByUser;
	
	@Column(name="updated_by_user_account__id")
	private Integer updatedByUserAccountid;
	
	@Column(name="updated_by_user_profile__id")
	private Integer updatedByUserProfileId;

	@Column(name="biller_image")
	private String billerImage;
	
	@Column(name="country__code")
	private String countryCode;

	@Column(name="atr2_name")
	private String atr2Name;
	
	@Column(name="atr3_Name")
	private String atr3Name;
	
	@Column(name="atr2_value")
	private String atr2Value;
	
	@Column(name="atr3_value")
	private String atr3Value;
	
	
	public String getAtr2Name() {
		return atr2Name;
	}

	public void setAtr2Name(String atr2Name) {
		this.atr2Name = atr2Name;
	}

	public String getAtr3Name() {
		return atr3Name;
	}

	public void setAtr3Name(String atr3Name) {
		this.atr3Name = atr3Name;
	}

	public String getBillerImage() {
		return billerImage;
	}

	public void setBillerImage(String billerImage) {
		this.billerImage = billerImage;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(Integer userProfileId) {
		this.userProfileId = userProfileId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getIdentifyId() {
		return identifyId;
	}

	public void setIdentifyId(String identifyId) {
		this.identifyId = identifyId;
	}

	public String getIdentifyType() {
		return identifyType;
	}

	public void setIdentifyType(String identifyType) {
		this.identifyType = identifyType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKhanCode() {
		return khanCode;
	}

	public void setKhanCode(String khanCode) {
		this.khanCode = khanCode;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getCommuneCode() {
		return communeCode;
	}

	public void setCommuneCode(String communeCode) {
		this.communeCode = communeCode;
	}

	public String getVillageCode() {
		return villageCode;
	}

	public void setVillageCode(String villageCode) {
		this.villageCode = villageCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractFile() {
		return contractFile;
	}

	public void setContractFile(String contractFile) {
		this.contractFile = contractFile;
	}

	public Date getContractDate() {
		return contractDate;
	}

	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getUserProfileRefId() {
		return userProfileRefId;
	}

	public void setUserProfileRefId(Integer userProfileRefId) {
		this.userProfileRefId = userProfileRefId;
	}

	public String getAtr1Name() {
		return atr1Name;
	}

	public void setAtr1Name(String atr1Name) {
		this.atr1Name = atr1Name;
	}

	public String getAtr1Value() {
		return atr1Value;
	}

	public void setAtr1Value(String atr1Value) {
		this.atr1Value = atr1Value;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Integer getCreatedByProfileId() {
		return createdByProfileId;
	}

	public void setCreatedByProfileId(Integer createdByProfileId) {
		this.createdByProfileId = createdByProfileId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedByStaffId() {
		return updatedByStaffId;
	}

	public void setUpdatedByStaffId(Integer updatedByStaffId) {
		this.updatedByStaffId = updatedByStaffId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getIsCreatedByUser() {
		return isCreatedByUser;
	}

	public void setIsCreatedByUser(String isCreatedByUser) {
		this.isCreatedByUser = isCreatedByUser;
	}

	public Integer getCreatedByUserAccountId() {
		return createdByUserAccountId;
	}

	public void setCreatedByUserAccountId(Integer createdByUserAccountId) {
		this.createdByUserAccountId = createdByUserAccountId;
	}

	public String getIsUpdatedByUser() {
		return isUpdatedByUser;
	}

	public void setIsUpdatedByUser(String isUpdatedByUser) {
		this.isUpdatedByUser = isUpdatedByUser;
	}

	public Integer getUpdatedByUserAccountid() {
		return updatedByUserAccountid;
	}

	public void setUpdatedByUserAccountid(Integer updatedByUserAccountid) {
		this.updatedByUserAccountid = updatedByUserAccountid;
	}

	public Integer getUpdatedByUserProfileId() {
		return updatedByUserProfileId;
	}

	public void setUpdatedByUserProfileId(Integer updatedByUserProfileId) {
		this.updatedByUserProfileId = updatedByUserProfileId;
	}
	@Column(name = "gender")
	private String gender;


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	@Column(name = "kyc_status")
	private String kycStatus;


	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	
	@Column(name="is_payroll_mail")
	private String isPayrollMail;

	@Column(name="is_payroll_sms")
	private String isPayrollSms;


	public String getIsPayrollMail() {
		return isPayrollMail;
	}

	public void setIsPayrollMail(String isPayrollMail) {
		this.isPayrollMail = isPayrollMail;
	}

	public String getIsPayrollSms() {
		return isPayrollSms;
	}

	public void setIsPayrollSms(String isPayrollSms) {
		this.isPayrollSms = isPayrollSms;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAtr2Value() {
		return atr2Value;
	}

	public void setAtr2Value(String atr2Value) {
		this.atr2Value = atr2Value;
	}

	public String getAtr3Value() {
		return atr3Value;
	}

	public void setAtr3Value(String atr3Value) {
		this.atr3Value = atr3Value;
	}
}
