package com.tmc.cache.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "user_wallet")
public class UserWallet implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "wallet_id")
	private Integer walletId;

	@Column(name = "user_account__id")
	private Integer userAccountId;

	@Column(name = "wallet_nickname")
	private String walletNickname;

	@Column(name = "wallet_currency__id")
	private String walletCurrencyId;

	@Column(name = "wallet_type__id")
	private String walletTypeId;

	@Column(name = "status")
	private String status;

	@Column(name = "last_transaction__id")
	private Integer lastTransactionId;

	@Column(name = "last_transaction_service_type__id")
	private String lastTransactionServiceTypeId;

	@Column(name = "last_transaction_on")
	private Date lastTransactionOn;

	@Column(name = "last_balance_credit")
	private Double lastBalanceCredit;

	@Column(name = "last_balance_debit")
	private Double lastBalanceDebit;

	@Column(name = "balance_credit")
	private Double balanceCredit;

	@Column(name = "prev_balance")
	private Double prevBalance;

	@Column(name = "post_balance")
	private Double postBalance;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "staff__id")
	private Integer staffId;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by_staff__id")
	private Integer updatedByStaffId;

	@Column(name = "created_by_user_profile__id")
	private Integer createdByUserProfileId;
	
	@Column(name = "sequence_number")
	private Integer sequenceNumber;

	@Column(name = "total_balance")
	private Double totalBalance;
	
	@Column(name = "fee_charge")
	private Double feeCharge;
	
	public Integer getCreatedByUserProfileId() {
		return createdByUserProfileId;
	}

	public void setCreatedByUserProfileId(Integer createdByUserProfileId) {
		this.createdByUserProfileId = createdByUserProfileId;
	}

	public Integer getWalletId() {
		return walletId;
	}

	public void setWalletId(Integer walletId) {
		this.walletId = walletId;
	}

	public String getWalletNickname() {
		return walletNickname;
	}

	public void setWalletNickname(String walletNickname) {
		this.walletNickname = walletNickname;
	}

	public String getWalletCurrencyId() {
		return walletCurrencyId;
	}

	public void setWalletCurrencyId(String walletCurrencyId) {
		this.walletCurrencyId = walletCurrencyId;
	}

	public String getWalletTypeId() {
		return walletTypeId;
	}

	public void setWalletTypeId(String walletTypeId) {
		this.walletTypeId = walletTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getLastTransactionId() {
		return lastTransactionId;
	}

	public void setLastTransactionId(Integer lastTransactionId) {
		this.lastTransactionId = lastTransactionId;
	}

	public String getLastTransactionServiceTypeId() {
		return lastTransactionServiceTypeId;
	}

	public void setLastTransactionServiceTypeId(
			String lastTransactionServiceTypeId) {
		this.lastTransactionServiceTypeId = lastTransactionServiceTypeId;
	}

	public Date getLastTransactionOn() {
		return lastTransactionOn;
	}

	public void setLastTransactionOn(Date lastTransactionOn) {
		this.lastTransactionOn = lastTransactionOn;
	}

	public Double getLastBalanceCredit() {
		return lastBalanceCredit;
	}

	public void setLastBalanceCredit(Double lastBalanceCredit) {
		this.lastBalanceCredit = lastBalanceCredit;
	}

	public Double getLastBalanceDebit() {
		return lastBalanceDebit;
	}

	public void setLastBalanceDebit(Double lastBalanceDebit) {
		this.lastBalanceDebit = lastBalanceDebit;
	}

	public Double getBalanceCredit() {
		return balanceCredit;
	}

	public void setBalanceCredit(Double balanceCredit) {
		this.balanceCredit = balanceCredit;
	}

	public Double getPrevBalance() {
		return prevBalance;
	}

	public void setPrevBalance(Double prevBalance) {
		this.prevBalance = prevBalance;
	}

	public Double getPostBalance() {
		//System.out.println("before convert:"+FormatDouble.convertWrong(postBalance));
		return postBalance;
	}

	public void setPostBalance(Double postBalance) {
		this.postBalance = postBalance;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedByStaffId() {
		return updatedByStaffId;
	}

	public void setUpdatedByStaffId(Integer updatedByStaffId) {
		this.updatedByStaffId = updatedByStaffId;
	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}

	public Double getFeeCharge() {
		return feeCharge;
	}

	public void setFeeCharge(Double feeCharge) {
		this.feeCharge = feeCharge;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserWallet [walletId=" + walletId + ", userAccountId="
				+ userAccountId + ", walletNickname=" + walletNickname
				+ ", walletCurrencyId=" + walletCurrencyId + ", walletTypeId="
				+ walletTypeId + ", status=" + status + ", lastTransactionId="
				+ lastTransactionId + ", lastTransactionServiceTypeId="
				+ lastTransactionServiceTypeId + ", lastTransactionOn="
				+ lastTransactionOn + ", lastBalanceCredit="
				+ lastBalanceCredit + ", lastBalanceDebit=" + lastBalanceDebit
				+ ", balanceCredit=" + balanceCredit + ", prevBalance="
				+ prevBalance + ", postBalance=" + postBalance + ", createdAt="
				+ createdAt + ", staffId=" + staffId + ", updatedAt="
				+ updatedAt + ", updatedByStaffId=" + updatedByStaffId
				+ ", createdByUserProfileId=" + createdByUserProfileId + "]";
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	public Double getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(Double totalBalance) {
		this.totalBalance = totalBalance;
	}
	
}
