package com.tmc.cache.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SysActivateDeviceDTO {

    private String phone;
    private String deviceType;
    private Integer accountId;
    private String cardId;

    public SysActivateDeviceDTO(String phone, String deviceType) {
        this.phone = phone;
        this.deviceType = deviceType;
    }

    public SysActivateDeviceDTO(String cardId, String deviceType, Integer accountId) {
        this.deviceType = deviceType;
        this.accountId = accountId;
        this.cardId = cardId;
    }

    public SysActivateDeviceDTO(Integer accountId, String deviceType, String cardId, String phone) {
        this.phone = phone;
        this.deviceType = deviceType;
        this.accountId = accountId;
        this.cardId = cardId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
