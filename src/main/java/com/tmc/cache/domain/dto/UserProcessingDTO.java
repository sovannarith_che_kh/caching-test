package com.tmc.cache.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * sovannarith on 9/9/18
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProcessingDTO {

    private Integer userAccountId;
    private String serviceProcessing;
    private String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt = new Date();

    public Integer getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Integer userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getServiceProcessing() {
        return serviceProcessing;
    }

    public void setServiceProcessing(String serviceProcessing) {
        this.serviceProcessing = serviceProcessing;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
