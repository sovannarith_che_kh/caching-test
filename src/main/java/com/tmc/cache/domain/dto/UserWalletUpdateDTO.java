package com.tmc.cache.domain.dto;

public class UserWalletUpdateDTO {

    private Integer userAccountId;
    private String walletCurrencyId;
    private Double postBalance;

    public Integer getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Integer userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getWalletCurrencyId() {
        return walletCurrencyId;
    }

    public void setWalletCurrencyId(String walletCurrencyId) {
        this.walletCurrencyId = walletCurrencyId;
    }

    public Double getPostBalance() {
        return postBalance;
    }

    public void setPostBalance(Double postBalance) {
        this.postBalance = postBalance;
    }
}
