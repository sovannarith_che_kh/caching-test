package com.tmc.cache.repo;

import com.tmc.cache.domain.SysActivateDevices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OtpRepo extends JpaRepository<SysActivateDevices, Long> {

    List<SysActivateDevices> getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(String phone, String status, String deviceType, Date expireDate);

    List<SysActivateDevices> getSysActivateDevicesByCardIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(String cardId, String status, String deviceType, Date expireDate);

    List<SysActivateDevices> getSysActivateDevicesByUserAccountIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(Integer accountId, String status, String deviceType, Date expireDate);

}
