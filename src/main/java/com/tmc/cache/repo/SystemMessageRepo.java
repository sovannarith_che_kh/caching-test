package com.tmc.cache.repo;

import com.tmc.cache.domain.SysMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * sovannarith on 9/5/18
 */
@Repository
public interface SystemMessageRepo extends JpaRepository<SysMessage, String> {

    List<SysMessage> findByCodeContainingIgnoreCase(String code);

    List<SysMessage> findByMessageContainingIgnoreCase(String message);

    List<SysMessage> findByCodeIn(List<String> codes);

}
