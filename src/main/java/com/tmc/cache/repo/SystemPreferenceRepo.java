package com.tmc.cache.repo;

import com.tmc.cache.domain.SystemPreference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * sovannarith on 9/5/18
 */
@Repository
public interface SystemPreferenceRepo extends JpaRepository<SystemPreference, String> {

    List<SystemPreference> findByVariableContainingIgnoreCase(String variable);

    List<SystemPreference> findByValueContainingIgnoreCase(String value);

    List<SystemPreference> findByVariableIn(List<String> variables);

}
