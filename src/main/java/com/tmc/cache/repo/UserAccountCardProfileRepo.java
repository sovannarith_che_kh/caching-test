package com.tmc.cache.repo;

import com.tmc.cache.domain.UserAccountCardProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * sovannarith on 9/15/18
 */
@Repository
public interface UserAccountCardProfileRepo extends JpaRepository<UserAccountCardProfile, Integer> {

    List<UserAccountCardProfile> getUserAccountCardProfileByUserAccountIdEqualsAndStatusEquals(Integer userAccountId, String status);

}
