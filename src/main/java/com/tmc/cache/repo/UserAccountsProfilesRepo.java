package com.tmc.cache.repo;

import com.tmc.cache.domain.UserAccountsProfiles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * sovannarith on 9/15/18
 */
@Repository
public interface UserAccountsProfilesRepo extends JpaRepository<UserAccountsProfiles, Integer> {

    List<UserAccountsProfiles> getUserAccountsProfilesByUserAccountIdEqualsAndStatusEquals(Integer userAccountId, String status);

}
