package com.tmc.cache.repo;

import com.tmc.cache.domain.UserAccountProcessing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * sovannarith on 9/7/18
 */
@Repository
public interface UserProcessingRepo extends JpaRepository<UserAccountProcessing, Integer> {

    @Modifying
    @Query("update UserAccountProcessing uap set uap.serviceProcessing = ?2, uap.status = ?3 where uap.userAccountId = ?1")
    public Integer updateUserProcessing(Integer userAccountId, String serviceProcessing, String status);

    public UserAccountProcessing getUserAccountProcessingByUserAccountId(Integer userAccountId);

}
