package com.tmc.cache.repo;

import com.tmc.cache.domain.UserWallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserWalletRepo extends JpaRepository<UserWallet, Integer> {

    List<UserWallet> getUserWalletByUserAccountId(Integer userAccountId);

    @Modifying
    @Query("update UserWallet uw set uw.postBalance = ?3 where uw.walletCurrencyId = ?2 and uw.userAccountId = ?1")
    Integer updateUserWalletByUserAccountIdAndCurrencyId(Integer userAccountId, String currencyId, Double postBalance);

}
