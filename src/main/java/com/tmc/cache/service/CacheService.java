package com.tmc.cache.service;

import com.tmc.cache.domain.SysMessage;
import com.tmc.cache.domain.SystemPreference;

import java.util.List;

/**
 * sovannarith on 9/5/18
 */
public interface CacheService {
    List<SystemPreference> getSysPreferenceByVariableContaining(String contain);

    List<SystemPreference> getSysPreferenceByValueContaining(String contain);

    List<SysMessage> getSysMessageByCodeContaining(String code);

    List<SysMessage> findByMessageContainingIgnoreCase(String message);

    List<SystemPreference> findByVariableIn(List<String> variables);

}
