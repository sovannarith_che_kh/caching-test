package com.tmc.cache.service;

import com.tmc.cache.domain.SysActivateDevices;
import com.tmc.cache.domain.dto.SysActivateDeviceDTO;

import java.util.List;

public interface OtpService {

    List<SysActivateDevices> getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO sysActivateDeviceDTO);

    List<SysActivateDevices> getSysActivateDevicesByCardIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO sysActivateDeviceDTO);

    List<SysActivateDevices> getSysActivateDevicesByUserAccountIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO sysActivateDeviceDTO);
}
