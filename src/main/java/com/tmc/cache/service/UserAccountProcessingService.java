package com.tmc.cache.service;

import com.tmc.cache.domain.UserAccountProcessing;

/**
 * sovannarith on 9/7/18
 */
public interface UserAccountProcessingService {

    public Integer updateUserProcessing(Integer userAccountId);

    public UserAccountProcessing getUserAccountProcessingByUserAccountId(Integer userAccountId);

}
