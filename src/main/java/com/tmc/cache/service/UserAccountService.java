package com.tmc.cache.service;

import com.tmc.cache.domain.UserAccountCardProfile;
import com.tmc.cache.domain.UserAccountsProfiles;

import java.util.List;

/**
 * sovannarith on 9/15/18
 */
public interface UserAccountService {

    UserAccountsProfiles getUserAccountsProfilesByUserAccountId(Integer userAccountId);

    UserAccountCardProfile getUserAccountCardProfileByUserAccountId(Integer userAccountId);

}
