package com.tmc.cache.service;

import com.tmc.cache.domain.UserWallet;
import com.tmc.cache.domain.dto.UserWalletUpdateDTO;

import java.util.List;

public interface UserWalletService {

    List<UserWallet> getUserWalletByUserAccountId(Integer userAccountId);

    Integer updateUserWalletByUserAccountIdAndCurrencyId(UserWalletUpdateDTO userWalletUpdateDTO);
}
