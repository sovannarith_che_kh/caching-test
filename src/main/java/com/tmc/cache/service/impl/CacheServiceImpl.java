package com.tmc.cache.service.impl;

import com.tmc.cache.domain.SysMessage;
import com.tmc.cache.domain.SystemPreference;
import com.tmc.cache.repo.SystemMessageRepo;
import com.tmc.cache.repo.SystemPreferenceRepo;
import com.tmc.cache.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * sovannarith on 9/5/18
 */
@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private SystemPreferenceRepo preferenceRepo;

    @Autowired
    private SystemMessageRepo messageRepo;

    @Override
    public List<SystemPreference> getSysPreferenceByVariableContaining(String contain) {
        if (contain.contains(",")){
            String[] strings = contain.split(",");
            return preferenceRepo.findByVariableIn(Arrays.asList(strings));
        }
        return preferenceRepo.findByVariableContainingIgnoreCase(contain);
    }

    @Override
    public List<SystemPreference> getSysPreferenceByValueContaining(String contain) {
        return preferenceRepo.findByValueContainingIgnoreCase(contain);
    }

    @Override
    public List<SysMessage> getSysMessageByCodeContaining(String code) {
        return messageRepo.findByCodeContainingIgnoreCase(code);
    }

    @Override
    public List<SysMessage> findByMessageContainingIgnoreCase(String message) {
        return messageRepo.findByMessageContainingIgnoreCase(message);
    }

    @Override
    public List<SystemPreference> findByVariableIn(List<String> variables) {
        return preferenceRepo.findByVariableIn(variables);
    }
}
