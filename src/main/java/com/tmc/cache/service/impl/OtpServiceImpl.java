package com.tmc.cache.service.impl;

import com.tmc.cache.domain.SysActivateDevices;
import com.tmc.cache.domain.dto.SysActivateDeviceDTO;
import com.tmc.cache.repo.OtpRepo;
import com.tmc.cache.service.OtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OtpServiceImpl implements OtpService {

    @Autowired
    private OtpRepo repo;

    @Override
    public List<SysActivateDevices> getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO dto) {
        return repo.getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(dto.getPhone(), "initiate", dto.getDeviceType(),new Date());
    }

    @Override
    public List<SysActivateDevices> getSysActivateDevicesByCardIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO dto) {
        return repo.getSysActivateDevicesByCardIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(dto.getCardId(), "initiate", dto.getDeviceType(),new Date());
    }

    @Override
    public List<SysActivateDevices> getSysActivateDevicesByUserAccountIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(SysActivateDeviceDTO dto) {
        return repo.getSysActivateDevicesByUserAccountIdAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual(dto.getAccountId(), "initiate", dto.getDeviceType(),new Date());
    }
}
