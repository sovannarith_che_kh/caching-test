package com.tmc.cache.service.impl;

import com.tmc.cache.domain.UserAccountProcessing;
import com.tmc.cache.repo.UserProcessingRepo;
import com.tmc.cache.service.UserAccountProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * sovannarith on 9/7/18
 */
@Service
public class UserAccountProcessingServiceImpl implements UserAccountProcessingService {

    @Autowired
    private UserProcessingRepo userProcessingRepo;

    @Override
    public Integer updateUserProcessing(Integer userAccountId) {
        return userProcessingRepo.updateUserProcessing(userAccountId, "no service", "PC");
    }

    @Override
    public UserAccountProcessing getUserAccountProcessingByUserAccountId(Integer userAccountId) {
        return userProcessingRepo.getUserAccountProcessingByUserAccountId(userAccountId);
    }
}
