package com.tmc.cache.service.impl;

import com.tmc.cache.domain.UserAccountCardProfile;
import com.tmc.cache.domain.UserAccountsProfiles;
import com.tmc.cache.repo.UserAccountCardProfileRepo;
import com.tmc.cache.repo.UserAccountsProfilesRepo;
import com.tmc.cache.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * sovannarith on 9/15/18
 */
@Service
public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    private UserAccountCardProfileRepo cardProfileRepo;

    @Autowired
    private UserAccountsProfilesRepo userAccountsProfilesRepo;

    @Override
    public UserAccountsProfiles getUserAccountsProfilesByUserAccountId(Integer userAccountId) {
        List<UserAccountsProfiles> list = userAccountsProfilesRepo.getUserAccountsProfilesByUserAccountIdEqualsAndStatusEquals(userAccountId, "active");
        if (!list.isEmpty()) return list.get(0);
        return null;
    }

    @Override
    public UserAccountCardProfile getUserAccountCardProfileByUserAccountId(Integer userAccountId) {
        List<UserAccountCardProfile> list = cardProfileRepo.getUserAccountCardProfileByUserAccountIdEqualsAndStatusEquals(userAccountId, "active");
        if (!list.isEmpty()) return list.get(0);
        return null;
    }
}
