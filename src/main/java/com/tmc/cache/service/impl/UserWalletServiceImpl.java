package com.tmc.cache.service.impl;

import com.tmc.cache.domain.UserWallet;
import com.tmc.cache.domain.dto.UserWalletUpdateDTO;
import com.tmc.cache.repo.UserWalletRepo;
import com.tmc.cache.service.UserWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserWalletServiceImpl implements UserWalletService {

    @Autowired
    UserWalletRepo walletRepo;

    @Override
    public List<UserWallet> getUserWalletByUserAccountId(Integer userAccountId) {
        return walletRepo.getUserWalletByUserAccountId(userAccountId);
    }

    @Override
    public Integer updateUserWalletByUserAccountIdAndCurrencyId(UserWalletUpdateDTO userWalletUpdateDTO) {
        return walletRepo.updateUserWalletByUserAccountIdAndCurrencyId(userWalletUpdateDTO.getUserAccountId(), userWalletUpdateDTO.getWalletCurrencyId(), userWalletUpdateDTO.getPostBalance());
    }

}
