package com.tmc.cache;

import com.tmc.cache.domain.UserAccountsProfiles;
import com.tmc.cache.repo.OtpRepo;
import com.tmc.cache.repo.UserAccountCardProfileRepo;
import com.tmc.cache.repo.UserAccountsProfilesRepo;
import com.tmc.cache.repo.UserWalletRepo;
import com.tmc.cache.service.CacheService;
import com.tmc.cache.service.UserAccountProcessingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * sovannarith on 9/5/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestApp {

    @Autowired
    public CacheService service;

    @Autowired
    public UserAccountProcessingService userAccountProcessingService;

    @Autowired
    public UserAccountCardProfileRepo repo;

    @Autowired
    public UserWalletRepo walletRepo;

    @Autowired
    public OtpRepo otpRepo;

    @Autowired
    public UserAccountsProfilesRepo userAccountsProfilesRepo;

    @Test
    public void tt(){
        List<String> list = Arrays.asList("url_mix_panel_service", "sms_services_mapping");
        service.findByVariableIn(list);
    }

    @Test
    @Transactional
    public void gg(){
//        userAccountProcessingService.updateUserProcessing(10004103);
        repo.getUserAccountCardProfileByUserAccountIdEqualsAndStatusEquals(10005021, "active");

        userAccountsProfilesRepo.getUserAccountsProfilesByUserAccountIdEqualsAndStatusEquals(23028000, "active");
    }

    @Test
    public void hh(){
        walletRepo.getUserWalletByUserAccountId(23028000);
    }

    @Test
    public void gG(){ // 1547777064756l
        otpRepo.getSysActivateDevicesByPhoneAndStatusAndDeviceTypeAndDateTimeExpireSecurityCodeGreaterThanEqual("011275847", "active", "mobile", new Date());
    }


}
